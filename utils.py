import pandas as pd


def save_to_xlsx(file: str, data: list):
    df = pd.DataFrame(data)
    df.to_excel(file, 'Кошелек ЦУПИС')
