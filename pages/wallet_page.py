from time import sleep
from selenium.webdriver.common.by import By
from pages.base import Base
import logging


class Locators:
    WALLET_LOGO = (By.XPATH, '/html/body/div/div[1]/header/div[2]/div[2]/a/img')
    WALLET_NAME_TEXT = (By.XPATH, './/div[@class="history-item"]/div/div[2]/div[contains(@class,"text-ellipsis")][1]')
    WALLET_OPERATION_TEXT = (By.XPATH, './/div[@class="history-item"]/div/div[2]/div[@class!="value"][2]')
    WALLET_MONEY_TEXT = (By.XPATH, './/div[@class="history-item"]/div/div[contains(text(),"₽")]')

    WALLET_NEXT_PAGE = (By.XPATH, '//*[@id="log_pager"]/div[3]/button')


class WalletPage(Base):
    def __init__(self, driver, url):
        super().__init__(driver, url)

    def __operations(self):
        operations = self.find_elements(Locators.WALLET_NAME_TEXT)
        operation_types = self.find_elements(Locators.WALLET_OPERATION_TEXT)
        operation_money = self.find_elements(Locators.WALLET_MONEY_TEXT)
        result = []
        for i, j, k in zip(operations, operation_types, operation_money):
            # self.scroll_to_element(k)
            # sleep(1)
            result.append((i.text, j.text, k.text))
        return result

    def get_operations(self):
        self.wait_element(Locators.WALLET_LOGO)
        operations = self.find_elements(Locators.WALLET_NAME_TEXT)
        logging.info(f'Количество проведенных операций {len(operations)}')
        res = []
        if len(operations) == 10:
            next_button = self.find_element(Locators.WALLET_NEXT_PAGE)
            next_is_clickable = self.is_clickable(Locators.WALLET_NEXT_PAGE)
            if next_is_clickable:
                logging.info(f'Кнопка Далее активна {next_is_clickable}')
                while self.is_clickable(Locators.WALLET_NEXT_PAGE):
                    res += self.__operations()
                    self.wait_and_click(Locators.WALLET_NEXT_PAGE)
                    self.wait_element(Locators.WALLET_LOGO)
                    print(res)
                res += self.__operations()
                print(res)
            else:
                res += self.__operations()
                print(res)
        else:
            res += self.__operations()
            print(res)
        for row in res:
            logging.info(row)
        return res
