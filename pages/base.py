import logging

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class Base:
    def __init__(self, driver: WebDriver, base_url: str):
        self.base_url = base_url
        self.driver = driver
        self.timeout = 5

    def wait_and_click(self, locator):
        return WebDriverWait(self.driver, 20, 5) \
            .until(ec.element_to_be_clickable(locator), message=f'Cant not find element by locator {locator}') \
            .click()

    def scroll_to_element(self, locator):
        self.driver.execute_script("arguments[0].scrollIntoView();", locator)

    def find_element(self, locator):
        return self.driver.find_element(locator[0], locator[1])

    def find_elements(self, locator):
        return self.driver.find_elements(locator[0], locator[1])

    def click(self, locator):
        return self.find_element(locator).click()

    def open(self, url: str = ''):
        url = self.base_url + url
        self.driver.get(url)

    def get_title(self):
        return self.driver.title

    def get_url(self):
        return self.driver.current_url

    def hover(self, locator):
        element = self.find_element(locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

    def is_clickable(self, locator):
        try:
            WebDriverWait(self.driver, self.timeout).until(ec.element_to_be_clickable(locator))
            return True
        except TimeoutException:
            return False

    def wait_element(self, locator):
        try:
            WebDriverWait(self.driver, self.timeout).until(ec.presence_of_element_located(locator))
        except TimeoutException:
            self.driver.get_screenshot_as_file('error.png')
            logging.info("\n * ELEMENT NOT FOUND WITHIN GIVEN TIME! --> %s" % (locator[1]))
            self.driver.quit()

