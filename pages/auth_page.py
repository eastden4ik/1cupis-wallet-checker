from time import sleep

from selenium.webdriver.common.by import By

from pages.base import Base


class Locators:
    OPEN_LOGIN_PAGE = (By.XPATH, './/a[@class="t-header__login-auth"]')
    LOGIN_TEXT_INPUT = (By.XPATH, '//*[@id="form_login_phone"]')
    PASSWORD_TEXT_INPUT = (By.XPATH, '//*[@id="form_login"]/div[1]/div[2]/input')
    SIGNIN_BUTTON = (By.XPATH, '//*[@id="btn_authorization_enter"]')


class AuthPage(Base):
    def __init__(self, driver, url):
        super().__init__(driver, url)

    def go_to_login_page(self):
        self.wait_element(Locators.OPEN_LOGIN_PAGE)
        self.click(Locators.OPEN_LOGIN_PAGE)

    def sign_in(self, login: str, password: str):
        self.driver.get_screenshot_as_png()
        self.wait_element(Locators.LOGIN_TEXT_INPUT)
        self.find_element(Locators.LOGIN_TEXT_INPUT).send_keys(login)
        self.find_element(Locators.PASSWORD_TEXT_INPUT).send_keys(password)
        self.click(Locators.SIGNIN_BUTTON)
