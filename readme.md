
# 1Cupis wallet checker

## Installation

``git clone git@gitlab.com:eastden4ik/1cupis-wallet-checker.git``

``cd 1cupis-wallet-checker``

``pip install -r requirements.txt``

## Pre-requirements

In `env.yaml` set login in format `9998170434` and password.

In folder drivers move actual chromedriver for your Chrome version.

## Run

run in cmd ``py main.py``