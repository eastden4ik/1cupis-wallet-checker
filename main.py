import logging
import sqlite3

from envyaml import EnvYAML
import undetected_chromedriver as uc
from random_user_agent.params import SoftwareName, OperatingSystem
from random_user_agent.user_agent import UserAgent
from selenium_stealth import stealth

from pages.auth_page import AuthPage
from pages.wallet_page import WalletPage
from utils import save_to_xlsx
log: logging = logging.getLogger(__name__)


def get_history(login: str, password: str):
    env = EnvYAML('env.yaml')
    software_names = [SoftwareName.CHROME.value, SoftwareName.EDGE.value, SoftwareName.FIREFOX.value,
                      SoftwareName.OPERA.value]
    operating_systems = [OperatingSystem.WINDOWS.value]
    user_agent_rotator = UserAgent(software_names=software_names, operating_systems=operating_systems,
                                   limit=100)  # popularity=popularity
    user_agent = user_agent_rotator.get_random_user_agent()

    opts = uc.ChromeOptions()
    opts.add_argument('--disable-extensions')
    opts.add_argument('--profile-directory=Default')
    opts.add_argument("--incognito")
    opts.add_argument("--disable-plugins-discovery")
    opts.add_argument('--no-sandbox')
    opts.add_argument('--headless')
    opts.add_argument('--enable-javascript')
    opts.add_argument('--disable-gpu')
    opts.add_argument('--ignore-certificate-errors')
    driver = uc.Chrome(use_subprocess=True, options=opts)

    stealth(driver,
            languages=["en-US", "en"],
            vendor="Google Inc.",
            platform="Win32",
            webgl_vendor="Intel Inc.",
            renderer="Intel Iris OpenGL Engine",
            fix_hairline=True,
            )

    driver.maximize_window()
    driver.execute_cdp_cmd("Network.setUserAgentOverride", {
        "userAgent": user_agent})
    driver.delete_all_cookies()

    auth = AuthPage(driver, env['url'])
    auth.open()
    logging.info(f'Открыта страница {env["url"]}')
    auth.go_to_login_page()
    logging.info('Выполнен переход на страницу авторизации')
    auth.sign_in(login, password)
    logging.info('Выполнена авторизация')
    wallet = WalletPage(auth.driver, auth.get_url())
    logging.info('Получение проведенных операций')
    operations = wallet.get_operations()
    log.info(str(operations))
    wallet.driver.quit()
    return operations


if __name__ == '__main__':
    env = EnvYAML('env.yaml')
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    handler = logging.FileHandler(env['logs'], 'a', 'utf-8')
    handler.setFormatter(logging.Formatter('%(asctime)s, %(msecs)d %(name)s %(levelname)s %(message)s'))
    root_logger.addHandler(handler)
    operations = get_history(env['login'], env['password'])
    save_to_xlsx(env['file'], operations)
